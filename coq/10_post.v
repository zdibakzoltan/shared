Require Import Nat.
Require Import Arith.

(* Require Import String. *)
Require Import Coq.Strings.String.
Require Import Coq.Lists.List.
Require Import Coq.Lists.ListSet.

Open Scope string_scope.
(* Open Scope list_scope. *)
Import ListNotations.


(* -=- Var -=- *)

Definition Var := string.
Definition Var_dec := string_dec.

Definition x := "x".
Definition y := "y".
Definition z := "z".

(* Opaque x y z. *)


(* -=- VarSet -=- *)

Module VarSet.

  (* -=- Definitions -=- *)

  Definition empty := empty_set Var.
  Definition add := set_add Var_dec.
  Definition mem := set_mem Var_dec.

  (* Definition remove := set_remove Var_dec. *)
  Fixpoint remove (v : Var) (s : set Var) : set Var :=
    match s with
    | nil => nil
    | v' :: s' =>
      match Var_dec v v' with
      | left _ => remove v s'
      | right _ => v' :: remove v s'
      end
    end
  .

  Definition inter := set_inter Var_dec.
  Definition union := set_union Var_dec.
  Definition diff := set_diff Var_dec.

  Definition singleton (v : Var) : set Var := add v empty.

  Definition Subset (s1 s2 : set Var) :=
    forall v : Var, In v s1 -> In v s2.

End VarSet.

(*
  The "Tm" type and the previously defined typing relation.
*)

Inductive Ty : Set :=
| Nat : Ty
| Bool : Ty
| Arrow : Ty -> Ty -> Ty
.

Inductive Con : Set :=
| empty
| extended (G : Con) (x : Var) (A : Ty)
.

Inductive Tm : Set :=
| num (n : nat)
| plus (t t' : Tm)
| isZero (t : Tm)
| true
| false
| ifThenElse (t t' t'' : Tm)

| variable (v : Var) : Tm
| letBind (t : Tm) (v : Var) (t' : Tm)

(* | lambda (ty : Ty) (v : Var) (t : Tm) : Tm *)
(* | application (t t' : Tm) : Tm *)
.


Notation "ty => ty'" := (Arrow ty ty') (at level 100, right associativity).

Notation "*" := empty.
Notation "G , x : A" := (extended G x A) (at level 50, x at next level).

(* Notation "( x : A ) 'E' G" := (extended G x A) *)
         (* (at level 10, x at level 99, A at level 99, G at level 99). *)
(* Check ( x : Bool E * ). *)

Notation "t + t'" := (plus t t').
Notation
  "'If' t 'then' t' 'else' t''" :=
  (ifThenElse t t' t'')
  (at level 100)
.

Notation "' v" := (variable v) (at level 3).
Notation
  "'Let' t 'in' v , t'" :=
  (letBind t v t')
  (at level 40, v at next level)
.

(* Notation "\ v , t" := (lambda v t) (at level 5). *)
(* Notation "t $ t'" := (application t t') (at level 4). *)

Check (Let false in x , true).

Fixpoint dom (G : Con) : set Var :=
  match G with
  | empty => VarSet.empty
  | extended G x A => VarSet.union (VarSet.singleton x) (dom G)
  end
.

Reserved Notation "G 'wf'" (at level 60).
Inductive WellFormedJudgement : Con -> Prop :=
| WFJ_empty : empty wf
| WFJ_extended {G : Con} {x : Var} {A : Ty} :
    (G wf) -> (~ In x (dom G)) ->
    (G,x:A wf)
where "G 'wf'" := (WellFormedJudgement G).

Lemma wfTest1 : * wf.
Proof.
  eapply WFJ_empty.
Qed.

Lemma wfTest2 : *,x:Nat,y:Bool wf.
Proof.
  eapply WFJ_extended.
  - eapply WFJ_extended.
    + eapply WFJ_empty.
    + exact id.
  - simpl.
    + unfold not. intros.
      destruct H.
      * inversion H.
      * inversion H.
Qed.

Lemma wfTest3 : ~ *,x:Nat,x:Bool wf.
Proof.
  unfold not. intros.
  inversion H.
  eapply H4.
  simpl. left. reflexivity.
Qed.

Reserved Notation "x : A '-<' G" (at level 100, A at next level).
Inductive Contains : Var -> Ty -> Con -> Prop :=
| C_Here
    {G : Con} {x : Var} {A : Ty} :
    (G wf) -> (~ In x (dom G)) ->
    (x:A -< (G,x:A))

| C_There
    {G : Con} {x y : Var} {A A' : Ty} :
    (x:A -< G) -> (~ In y (dom G)) ->
    (x:A -< (G,y:A'))
where "x : A '-<' G" := (Contains x A G).

(* The inductive typing relation. *)

Reserved Notation "G |- tm : ty" (at level 100, tm at next level).
Inductive TypeJudgement : Con -> Tm -> Ty -> Prop :=
| TJ_num {G : Con} {n : nat} :
    (G wf) ->
    G |- (num n) : Nat

| TJ_plus {G : Con} {t t' : Tm} :
    (G |- t : Nat) -> (G |- t' : Nat) ->
    G |- (t + t') : Nat

| TJ_isZero {G : Con} {t : Tm} :
    (G |- t : Nat) ->
    G |- (isZero t) : Bool

| TJ_true {G : Con} :
    (G wf) ->
    G |- true : Bool

| TJ_false {G : Con} :
    (G wf) ->
    G |- false : Bool

| TJ_ifThenElse
    {G : Con} {t t' t'' : Tm} {A : Ty} :
    (G |- t : Bool) -> (G |- t' : A) -> (G |- t'' : A) ->
    G |- (If t then t' else t'') : A


| TJ_variable
    {G : Con} {x : Var} {A : Ty} :
    (x : A -< G) ->
    G |- 'x : A

| TJ_letBind
    {G : Con} {x : Var} {t1 t2 : Tm} {A1 A2 : Ty} :
    (G |- t1 : A1) -> (G , x : A1 |- t2 : A2) -> (~ In x (dom G)) ->
    G |- Let t1 in x , t2 : A2

where "G |- tm : ty" := (TypeJudgement G tm ty).

Lemma typeJudgementTest :
  * |- Let (num 3) in x,isZero ('x + num 2) : Bool.
Proof.
  eapply TJ_letBind. (* 4.15 *)
  - eapply TJ_num. (* 4.8 *)
    + eapply WFJ_empty. (* 4.4 *)
  - eapply TJ_isZero. (* 4.10 *)
    + eapply TJ_plus. (* 4.9 *)
      * eapply TJ_variable. (* 4.14 *)
        -- eapply C_Here. (* 4.6 *)
           ++ eapply WFJ_empty. (* 4.4 *)
           ++ simpl. exact id.
      * eapply TJ_num. (* 4.8 *)
        -- eapply WFJ_extended. (* 4.5 *)
           ++ eapply WFJ_empty. (* 4.4 *)
           ++ simpl. exact id.
  - simpl. exact id.
Qed.