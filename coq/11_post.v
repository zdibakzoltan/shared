Require Import Nat.
Require Import Arith.

(* Require Import String. *)
Require Import Coq.Strings.String.
Require Import Coq.Lists.List.
Require Import Coq.Lists.ListSet.

Open Scope string_scope.
(* Open Scope list_scope. *)
Import ListNotations.


(* -=- Var -=- *)

Definition Var := string.
Definition Var_dec := string_dec.

Definition x := "x".
Definition y := "y".
Definition z := "z".


(* -=- VarSet -=- *)

Module VarSet.

  (* -=- Definitions -=- *)

  Definition empty := empty_set Var.
  Definition add := set_add Var_dec.
  Definition mem := set_mem Var_dec.

  (* Definition remove := set_remove Var_dec. *)
  Fixpoint remove (v : Var) (s : set Var) : set Var :=
    match s with
    | nil => nil
    | v' :: s' =>
      match Var_dec v v' with
      | left _ => remove v s'
      | right _ => v' :: remove v s'
      end
    end
  .

  Definition inter := set_inter Var_dec.
  Definition union := set_union Var_dec.
  Definition diff := set_diff Var_dec.

  Definition singleton (v : Var) : set Var := add v empty.

  Definition Subset (s1 s2 : set Var) :=
    forall v : Var, In v s1 -> In v s2.

  Lemma not_in_union {v : Var} {s1 s2 : set Var} :
    ~ In v (union s1 s2) <-> ~ In v s1 /\ ~ In v s2.
  Proof.
    split.
      - intros. unfold not in *. split.
        + intros. eapply H. eapply set_union_iff. left. exact H0.
        + intros. eapply H. eapply set_union_iff. right. exact H0.
      - intros. inversion H. unfold not. intros.
        eapply set_union_iff in H2. destruct H2.
        + exact (H0 H2).
        + exact (H1 H2).
  Qed.

  Lemma in_singleton {v1 v2 : Var} : In v1 (singleton v2) <-> v1 = v2.
  Proof.
    split.
      - simpl. intros. inversion H.
        + symmetry. exact H0.
        + inversion H0.
      - intros. subst. simpl. left. reflexivity.
  Qed.

  Lemma not_in_singleton {v1 v2 : Var} : ~ In v1 (singleton v2) <-> v1 <> v2.
  Proof.
    eapply not_iff_compat. exact in_singleton.
  Qed.

End VarSet.


(* -=- Ty -=- *)

Inductive Ty : Set :=
| Nat : Ty
| Bool : Ty
| Arrow : Ty -> Ty -> Ty
.

Notation "ty => ty'" := (Arrow ty ty') (at level 100, right associativity).

Check ((Bool => Bool) => Bool).

(* -=- Tm -=- *)

Inductive Tm : Set :=
| num (n : nat)
| plus (t t' : Tm)
| isZero (t : Tm)
| true
| false
| ifThenElse (t t' t'' : Tm)

| variable (v : Var) : Tm
| lambda (v : Var) (A : Ty) (t : Tm) : Tm
| application (t t' : Tm) : Tm
.

Notation "t + t'" := (plus t t').
Notation "'If' t 'then' t' 'else' t''" := (ifThenElse t t' t'') (at level 100).

Notation "' v" := (variable v) (at level 3).
Notation "\ v : A , t" := (lambda v A t) (at level 5, v at next level, A at next level).
Notation "t $ t'" := (application t t') (at level 4).


(* -=- Con -=- *)

Inductive Con : Set :=
| empty
| extended (G : Con) (x : Var) (A : Ty)
.

Notation "*" := empty.
Notation "G , x : A" := (extended G x A) (at level 50, x at next level).


(* -=- Rename -=- *)

Reserved Notation "t { x |-> y }" (at level 1).

(* Print Grammar constr. *)
(* Print Grammar pattern. *)

Fixpoint rename (tm : Tm) (y z : Var) : Tm :=
  match tm with
  | num n => num n
  | t + t' => t{y|->z} + t'{y|->z}
  | isZero t => isZero t{y|->z}
  | true => true
  | false => false
  | (If t then t' else t'') => If t{y|->z} then t'{y|->z} else t''{y|->z}

  | ' x => if x =? y then 'z else 'x
  | \ x : A , t => if x =? y then \z:A,t{y|->z} else \x:A,t{y|->z}
  | t $ t' => t{y|->z} $ t'{y|->z}
  end
where "t { y |-> z }" := (rename t y z).


(* -=- Substitute -=- *)

Reserved Notation "t [ x |-> t' ]" (at level 2).

Fixpoint substitute (tm : Tm) (x : Var) (tm' : Tm) : Tm :=
  match tm with
  | num n => num n
  | t + t' => t[x|->tm'] + t'[x|->tm']
  | isZero t => isZero t[x|->tm']
  | true => true
  | false => false
  | (If t then t' else t'') => If t[x|->tm'] then t'[x|->tm'] else t''[x|->tm']

  | ' y => if x =? y then tm' else ' y
  | \ y : A , t => if x =? y then \y:A,t else \y:A,t[x|->tm']
  | t $ t' => t[x|->tm'] $ t'[x|->tm']
  end
where "t [ x |-> t' ]" := (substitute t x t').


(* -=- context -=- *)

Definition context := list Var.

(* -=- Closed -=- *)

Inductive Closed' : context -> Tm -> Prop :=
| C_num {c : context} {n : nat} : Closed' c (num n)
| C_plus {c : context} {t t' : Tm} : Closed' c t -> Closed' c t' -> Closed' c (t + t')
| C_isZero {c : context} {t : Tm} : Closed' c t -> Closed' c (isZero t)
| C_true {c : context} : Closed' c true
| C_false {c : context} : Closed' c false
| C_ifThenElse {c : context} {t t' t'' : Tm} :
    Closed' c t -> Closed' c t' -> Closed' c t'' -> Closed' c (If t then t' else t'')

| C_variable {c : context} {v : Var} : In v c -> Closed' c ('v)
| C_lambda {c : context} {v : Var} {A : Ty} {t : Tm} : Closed' (v :: c) t -> Closed' c (\v:A,t)
| C_application {c : context} {t t' : Tm} : Closed' c t -> Closed' c t' -> Closed' c (t $ t')
.

Definition Closed := Closed' nil.


(* -=- Value -=- *)

Reserved Notation "t 'val'" (at level 1).

Inductive ValueJudgement : Tm -> Prop :=
  | VJ_num {n : nat} : (num n) val
  | VJ_true : true val
  | VJ_false : false val
  | VJ_lambda {x : Var} {A : Ty} {t : Tm} : Closed (\x:A,t) -> (\x:A,t) val
where "t 'val'" := (ValueJudgement t).


(* -=- dom -=- *)

Fixpoint dom (G : Con) : set Var :=
  match G with
  | empty => VarSet.empty
  | extended G x A => VarSet.union (VarSet.singleton x) (dom G)
  end
.


(* -=- WellFormed -=- *)

Reserved Notation "G 'wf'" (at level 60).
Inductive WellFormedJudgement : Con -> Prop :=
| WFJ_empty : empty wf
| WFJ_extended {G : Con} {x : Var} {A : Ty} :
    (G wf) -> (~ In x (dom G)) ->
    (G,x:A wf)
where "G 'wf'" := (WellFormedJudgement G).


(* -=- Contains -=- *)

Reserved Notation "x : A '-<' G" (at level 100, A at next level).
Inductive Contains : Var -> Ty -> Con -> Prop :=
| C_Here
    {G : Con} {x : Var} {A : Ty} :
    (G wf) -> (~ In x (dom G)) ->
    (x:A -< (G,x:A))

| C_There
    {G : Con} {x y : Var} {A A' : Ty} :
    (x:A -< G) -> (~ In y (dom G)) ->
    (x:A -< (G,y:A'))
where "x : A '-<' G" := (Contains x A G).

Lemma containsWellFormed
  {x : Var} {A : Ty} {G : Con} :
  (x:A -< G) -> G wf.
Proof.
  intros. induction H; eapply WFJ_extended; assumption.
Qed.


(* The inductive typing relation. *)

Reserved Notation "G |- tm : ty" (at level 100, tm at next level).
Inductive TypeJudgement : Con -> Tm -> Ty -> Prop :=
| TJ_num {G : Con} {n : nat} :
    (G wf) ->
    G |- (num n) : Nat

| TJ_plus {G : Con} {t t' : Tm} :
    (G |- t : Nat) -> (G |- t' : Nat) ->
    G |- (t + t') : Nat

| TJ_isZero {G : Con} {t : Tm} :
    (G |- t : Nat) ->
    G |- (isZero t) : Bool

| TJ_true {G : Con} :
    (G wf) ->
    G |- true : Bool

| TJ_false {G : Con} :
    (G wf) ->
    G |- false : Bool

| TJ_ifThenElse
    {G : Con} {t t' t'' : Tm} {A : Ty} :
    (G |- t : Bool) -> (G |- t' : A) -> (G |- t'' : A) ->
    G |- (If t then t' else t'') : A


| TJ_variable
    {G : Con} {x : Var} {A : Ty} :
    (x : A -< G) ->
    G |- 'x : A

| TJ_lambda
    {G : Con} {x : Var} {t2 : Tm} {A1 A2 : Ty} :
    (G , x : A1 |- t2 : A2) ->
    G |- (\ x : A1 , t2) : (A1 => A2)

| TJ_application
    {G : Con} {t t1 : Tm} {A1 A2 : Ty} :
    (G |- t : (A1 => A2)) -> (G |- t1 : A1) ->
    (G |- t $ t1 : A2)

where "G |- tm : ty" := (TypeJudgement G tm ty).

Lemma typeJudgementWellFormed
  {t : Tm} {A : Ty} {G : Con} :
  (G |- t : A) -> G wf.
Proof.
  intros. induction H; try(assumption).
  - eapply containsWellFormed. exact H.
  - inversion IHTypeJudgement. assumption.
Qed.

Inductive Permutation : Con -> Con -> Prop :=
| perm_nil: Permutation * *
| perm_skip x A c1 c2 : Permutation c1 c2 -> Permutation (c1,x:A) (c2,x:A)
| perm_swap x A y A' c : Permutation (c,x:A,y:A') (c,y:A',x:A)
| perm_trans c1 c2 c3 :
    Permutation c1 c2 -> Permutation c2 c3 -> Permutation c1 c3.

Notation "c1 ~= c2" := (Permutation c1 c2) (at level 100).

Lemma permutationReflexivity {G : Con} : G ~= G.
Proof.
  induction G.
  - exact perm_nil.
  - eapply perm_skip. exact IHG.
Qed.

Lemma permutationSymmetry {G G' : Con} : (G ~= G') -> (G' ~= G).
Proof.
  intros.
  induction H.
  - exact perm_nil.
  - eapply perm_skip. assumption.
  - eapply perm_swap.
  - eapply perm_trans.
    + exact IHPermutation2.
    + exact IHPermutation1.
Qed.

Fixpoint length (G : Con) : nat :=
  match G with
  | * => 0 
  | G,x:A => 1 + (length G)
  end
.

Lemma permutationLength {G G' : Con} :
  (G ~= G') -> (length G = length G').
Proof.
  intros.
  induction H.
  - simpl. reflexivity.
  (* - simpl. congruence. *)
  - simpl. rewrite IHPermutation. reflexivity.
  - simpl. reflexivity.
  - rewrite IHPermutation1.
    rewrite IHPermutation2.
    reflexivity.
Qed.

Lemma permutationNotIn {G G' : Con} {x : Var} :
  ~ In x (dom G) -> (G ~= G') -> ~ In x (dom G').
Proof.
  intros.
  induction H0.
  - exact H.
  - destruct (Var_dec x x0).
    + subst. exfalso. eapply H.
      simpl. eapply set_union_iff. left. simpl. left. reflexivity.
    + simpl. eapply VarSet.not_in_union. split.
      * eapply VarSet.not_in_singleton. exact n.
      * eapply IHPermutation. simpl in H.
        eapply VarSet.not_in_union in H. inversion H. exact H2.
  - unfold not. intros. eapply H. simpl in *.
    eapply set_union_iff in H0. destruct H0.
    + eapply VarSet.in_singleton in H0. subst.
      eapply set_union_iff. right.
      eapply set_union_iff. left. simpl. left. reflexivity.
    + eapply set_union_iff in H0. destruct H0.
      * eapply VarSet.in_singleton in H0. subst.
        eapply set_union_iff. left. simpl. left. reflexivity.
      * eapply set_union_iff. right.
        eapply set_union_iff. right. exact H0.
  - eapply IHPermutation2. eapply IHPermutation1. exact H.
Qed.

Lemma permutationWellFormed {G G' : Con} :
  G wf -> (G ~= G') -> G' wf.
Proof.
  intros.
  induction H0.
  - exact WFJ_empty.
  - inversion H; subst. eapply WFJ_extended.
    + exact (IHPermutation H3).
    + exact (permutationNotIn H5 H0).
  - inversion H; inversion H2; subst. eapply WFJ_extended.
    + eapply WFJ_extended.
      * exact H7.
      * eapply VarSet.not_in_union. exact H4.
    + eapply VarSet.not_in_union. split.
      * unfold not. intros. eapply VarSet.in_singleton in H0.
        subst. eapply H4. simpl. eapply set_union_iff.
        left. simpl. left. reflexivity.
      * exact H9.
  - eapply IHPermutation2. eapply IHPermutation1. exact H.
Qed.

Lemma permutationContains {c1 c2 : Con} {v : Var} {A : Ty} :
  (c1 ~= c2) -> (v:A-<c1) -> (v:A-<c2).
Proof.
  intros. induction H.
  - inversion H0.
  - inversion H0; subst.
    + eapply C_Here.
      * exact (permutationWellFormed H5 H).
      * exact (permutationNotIn H7 H).
    + eapply C_There.
      * eapply IHPermutation. exact H5.
      * exact (permutationNotIn H7 H).
  - inversion H0; inversion H4; subst;
      assert (~ In y0 (dom c)) by (
        eapply VarSet.not_in_union in H6; exact (proj2 H6)
      ).
    + eapply C_There.
      * eapply C_Here; assumption.
      * shelve.
    + eapply C_Here.
      * eapply WFJ_extended; assumption.
      * shelve.
    + eapply C_There.
      * eapply C_There; assumption.
      * shelve.
  - eapply IHPermutation2. eapply IHPermutation1. exact H0.

    Unshelve. all: (
      eapply VarSet.not_in_union; split; try (assumption);
      eapply VarSet.not_in_singleton; unfold not; intros; subst;
      eapply H6; simpl; eapply set_union_iff; left; simpl; left; reflexivity
    ).
Qed.


(* -=- One Step Transition Judgement -=- *)

Reserved Notation "t |--> t'" (at level 100).

Inductive OneStepTransitionJudgement : Tm -> Tm -> Prop :=
| OSTJ_sum {n1 n2 n : nat} :
    ((n1 + n2)%nat = n) ->
    num n1 + num n2 |--> num n

| OSTJ_isZero_true :
    (isZero (num 0)) |--> true

| OSTJ_isZero_false {n : nat} :
    (n > 0) ->
    isZero (num n) |--> false

| OSTJ_ifThenElse_true {t t' : Tm} :
    (If true then t else t') |--> t

| OSTJ_ifThenElse_false {t t' : Tm} :
    (If false then t else t' )|--> t'

| OSTJ_plus_left {t1 t1' t2 : Tm} :
    (t1 |--> t1') ->
    t1 + t2 |--> t1' + t2

| OSTJ_plus_right {t1 t2 t2' : Tm} :
    t1 val -> (t2 |--> t2') ->
    t1 + t2 |--> t1 + t2'

| OSTJ_isZero {t t' : Tm} :
    (t |--> t') ->
    isZero t |--> isZero t'

| OSTJ_ifThenElse {t t' t1 t2 : Tm} :
    (t |--> t') ->
    (If t then t1 else t2) |--> (If t' then t1 else t2)

| OSTJ_application_left {t t' t1 : Tm} :
    (t |--> t') ->
    (t $ t1) |--> (t' $ t1)

| OSTJ_application_right {t t1 t1' : Tm} :
    (t val) -> (t1 |--> t1') ->
    (t $ t1) |--> (t $ t1')

| OSTJ_lambda {t1 t2 : Tm} {x : Var} {A : Ty} :
    (t1 val) ->
    (\x:A,t2) $ t1 |--> t2[x|->t1]

where "t |--> t'" := (OneStepTransitionJudgement t t').

(* -=- Any Step Transition Judgement -=- *)

Reserved Notation "t |-->* t'" (at level 100).

Inductive AnyStepTransitionJudgement : Tm -> Tm -> Prop :=
| ASTJ_refl {t : Tm} :
    t |-->* t
| ASTJ_trans {t t'' : Tm} (t' : Tm) : 
  (t |--> t') -> (t' |-->* t'') ->
  t |-->* t''
where "t |-->* t'" := (AnyStepTransitionJudgement t t').


(* T / 6. No val transition. *)
Lemma no_val_transition {t t' : Tm} :
  ~ ((t val) /\ (t |--> t'))
.
Proof.
  unfold not. intros. inversion H.
  induction H1; inversion H0.
Qed.









