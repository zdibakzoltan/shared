Set Warnings "-notation-overridden,-parsing".
From Coq Require Import Bool.Bool.
From Coq Require Import Init.Nat.
From Coq Require Import Strings.String.

Inductive aexp : Type :=
  | ALit (n : nat)
  | AVar (x : string)
  | APlus (a1 a2 : aexp)
  | AMinus (a1 a2 : aexp)
  | AMult (a1 a2 : aexp).

Inductive bexp : Type :=
  | BTrue
  | BFalse
  | BEq (a1 a2 : aexp)
  | BLe (a1 a2 : aexp)
  | BNot (b : bexp)
  | BAnd (b1 b2 : bexp).

Inductive stmt : Type :=
  | SSkip
  | SAssign (x: string) (a: aexp)
  | SSeq    (s1 s2: stmt)
  | SIf     (b: bexp) (s1 s2: stmt)
  | SWhile  (b: bexp) (s: stmt).

Definition X : string := "X".
Definition Y : string := "Y".
Definition Z : string := "Z".

Definition state : Type := string -> nat.
Definition empty : state := fun x => 0.
Definition update (x : string)(n : nat)(s : state): state := fun x' => if eqb x x' then n else s x'.

Fixpoint aeval (a : aexp) (st : state) : nat :=
  match a with
  | ALit n => n
  | AVar x => st x
  | APlus a1 a2 => (aeval a1 st) + (aeval a2 st)
  | AMinus a1 a2 => (aeval a1 st) - (aeval a2 st)
  | AMult a1 a2 => (aeval a1 st) * (aeval a2 st)
  end.

Fixpoint beval (b : bexp) (st : state) : bool :=
  match b with
  | BTrue => true
  | BFalse => false
  | BEq a1 a2 => (aeval a1 st) =? (aeval a2 st)
  | BLe a1 a2 => (aeval a1 st) <=? (aeval a2 st)
  | BNot b1 => negb (beval b1 st)
  | BAnd b1 b2 => andb (beval b1 st) (beval b2 st)
  end.

Reserved Notation "c -=> n" (at level 90).
Inductive eval_bigstep : stmt * state -> state -> Prop :=
| eval_skip st:
  (SSkip, st) -=> st
| eval_assign x a st:
  (SAssign x a, st) -=> update x (aeval a st) st
| eval_seq st' st st'' s1 s2:
  (s1, st) -=> st' ->
  (s2, st') -=> st'' ->
  (SSeq s1 s2, st) -=> st''
| eval_if_true st st' b s1 s2:
  (s1, st) -=> st' ->
  beval b st = true ->
  (SIf b s1 s2, st) -=> st'
| eval_if_false st st' b s1 s2:
  (s2, st) -=> st' ->
  beval b st = false ->
  (SIf b s1 s2, st) -=> st'
| eval_while_true st' st st'' b s:
  (s, st) -=> st' ->
  (SWhile b s, st') -=> st'' ->
  beval b st = true ->
  (SWhile b s, st) -=> st''
| eval_while_false b s st:
  beval b st = false ->
  (SWhile b s, st) -=> st
where "c -=> n" := (eval_bigstep c n).

Example e1:
  forall n:nat, forall st st':state,
  (SAssign X (APlus (AVar X) (ALit 1)), st) -=> st' ->
  st X = n ->
  st' X = n + 1.
Proof.
  intros.
  inversion H.
  subst.
  simpl.
  unfold update.
  reflexivity.
Qed.

Example e2:
  forall st st':state,
  st X = 42 ->
  (SWhile (BLe (AVar X) (ALit 1)) (SAssign X (ALit 42)), st) -=> st' ->
  st' X = 42.
Proof.
  intros.
  inversion H0.
  subst.
  simpl in H7.
  rewrite H in H7.
  simpl in H7.
  discriminate.
  rewrite <- H2.
  apply H.
Qed.