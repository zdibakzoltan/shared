Set Warnings "-notation-overridden,-parsing".
From Coq Require Import Bool.Bool.
From Coq Require Import Init.Nat.
From Coq Require Import Strings.String.

Inductive aexp : Type :=
  | ALit (n : nat)
  | AVar (x : string)
  | APlus (a1 a2 : aexp)
  | AMinus (a1 a2 : aexp)
  | AMult (a1 a2 : aexp).

Inductive bexp : Type :=
  | BTrue
  | BFalse
  | BEq (a1 a2 : aexp)
  | BLe (a1 a2 : aexp)
  | BNot (b : bexp)
  | BAnd (b1 b2 : bexp).

Inductive stmt : Type :=
  | SSkip
  | SAssign (x: string) (a: aexp)
  | SSeq    (s1 s2: stmt)
  | SIf     (b: bexp) (s1 s2: stmt)
  | SWhile  (b: bexp) (s: stmt)
  | SFor    (x: string) (a1 a2: aexp) (s: stmt).

Definition X : string := "X".
Definition Y : string := "Y".
Definition Z : string := "Z".

Definition state : Type := string -> nat.
Definition empty : state := fun x => 0.
Definition update (x : string)(n : nat)(s : state): state := fun x' => if eqb x x' then n else s x'.

Fixpoint aeval (a : aexp) (st : state) : nat :=
  match a with
  | ALit n => n
  | AVar x => st x
  | APlus a1 a2 => (aeval a1 st) + (aeval a2 st)
  | AMinus a1 a2 => (aeval a1 st) - (aeval a2 st)
  | AMult a1 a2 => (aeval a1 st) * (aeval a2 st)
  end.

Fixpoint beval (b : bexp) (st : state) : bool :=
  match b with
  | BTrue => true
  | BFalse => false
  | BEq a1 a2 => (aeval a1 st) =? (aeval a2 st)
  | BLe a1 a2 => (aeval a1 st) <=? (aeval a2 st)
  | BNot b1 => negb (beval b1 st)
  | BAnd b1 b2 => andb (beval b1 st) (beval b2 st)
  end.

Reserved Notation "c -=> n" (at level 90).
Inductive eval_bigstep : stmt * state -> state -> Prop :=
| eval_skip st:
  (SSkip, st) -=> st
| eval_assign x a st:
  (SAssign x a, st) -=> update x (aeval a st) st
| eval_seq st' st st'' s1 s2:
  (s1, st) -=> st' ->
  (s2, st') -=> st'' ->
  (SSeq s1 s2, st) -=> st''
| eval_if_true st st' b s1 s2:
  (s1, st) -=> st' ->
  beval b st = true ->
  (SIf b s1 s2, st) -=> st'
| eval_if_false st st' b s1 s2:
  (s2, st) -=> st' ->
  beval b st = false ->
  (SIf b s1 s2, st) -=> st'
| eval_while_true st' st st'' b s:
  (s, st) -=> st' ->
  (SWhile b s, st') -=> st'' ->
  beval b st = true ->
  (SWhile b s, st) -=> st''
| eval_while_false b s st:
  beval b st = false ->
  (SWhile b s, st) -=> st
where "c -=> s" := (eval_bigstep c s).

Lemma while_unfold (b:bexp) (s:stmt) (st st': state):
  (SWhile b s, st) -=> st' <->
    (SIf b (SSeq s (SWhile b s)) SSkip, st) -=> st'.
Proof.
  split.
  - intro. inversion H.
    * subst. apply eval_if_true.
(*assert ((SSeq s (SWhile b s), st) -=> st').*) (*apply (eval_seq st'0 _ _ _ _ H3 H5).*)
      apply eval_seq with st'0. assumption. assumption. (* exact H3. exact H5. *)
      exact H6.
    * subst. apply eval_if_false.
      apply eval_skip. exact H4.
  - intro. inversion H.
    * subst. inversion H5. subst. apply eval_while_true with st'0. exact H4. exact H7. exact H6.
    * subst. inversion H5. apply eval_while_false. rewrite H0 in H6. exact H6.
Qed.

-------------------------------------------------------------------------------------------

From Coq Require Import Bool.Bool.
From Coq Require Import Init.Nat.
From Coq Require Import Strings.String.

Inductive aexp : Type :=
  | ALit (n : nat)
  | AVar (x : string)
  | APlus (a1 a2 : aexp)
  | AMinus (a1 a2 : aexp)
  | AMult (a1 a2 : aexp).

Inductive bexp : Type :=
  | BTrue
  | BFalse
  | BEq (a1 a2 : aexp)
  | BLe (a1 a2 : aexp)
  | BNot (b : bexp)
  | BAnd (b1 b2 : bexp).

Inductive stmt : Type :=
  | SSkip
  | SAssign (x: string) (a: aexp)
  | SSeq    (s1 s2: stmt)
  | SIf     (b: bexp) (s1 s2: stmt)
  | SWhile  (b: bexp) (s: stmt).

Definition X : string := "X".
Definition Y : string := "Y".
Definition Z : string := "Z".

Definition state : Type := string -> nat.
Definition empty : state := fun x => 0.
Definition update (x : string)(n : nat)(s : state): state := fun x' => if eqb x x' then n else s x'.

Fixpoint aeval (a : aexp) (st : state) : nat :=
  match a with
  | ALit n => n
  | AVar x => st x
  | APlus a1 a2 => (aeval a1 st) + (aeval a2 st)
  | AMinus a1 a2 => (aeval a1 st) - (aeval a2 st)
  | AMult a1 a2 => (aeval a1 st) * (aeval a2 st)
  end.

Fixpoint beval (b : bexp) (st : state) : bool :=
  match b with
  | BTrue => true
  | BFalse => false
  | BEq a1 a2 => (aeval a1 st) =? (aeval a2 st)
  | BLe a1 a2 => (aeval a1 st) <=? (aeval a2 st)
  | BNot b1 => negb (beval b1 st)
  | BAnd b1 b2 => andb (beval b1 st) (beval b2 st)
  end.

From Coq Require Import Program.Basics. (* compose *)
From Coq Require Import funind.Recdef. (* iter *)

Definition id_state (s:state) : state := s.

(* The cond function, as seen in the lecture *)
Definition cond (p: state -> bool)(g1: state -> state) (g2: state -> state) : (state -> state) :=
  fun st => if p st then g1 st else g2 st.

(* We use iteration to mimic the fixed-point operator *)
Definition fixp := fun f => iter (state->state) 1024 f id_state.

(* Denotational semantics, as written in the lecture *)
Fixpoint eval (s: stmt) : state -> state :=
  match s with
  | SSkip => id_state
  | SAssign x a => fun st => update x (aeval a st) st
  | SSeq s1 s2 => compose (eval s1) (eval s2)
  | SIf b s1 s2 => cond (beval b) (eval s1) (eval s2)
  | SWhile b s => let F g := cond (beval b) (compose g (eval s)) id_state in fixp F
  end.
  (* TODO define this function just as we defined it in the lecture slides, use the above helpers *)