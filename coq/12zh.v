Require Import Coq.Arith.Arith.
Require Import Coq.Strings.String.

Definition state : Type := string -> nat.

Definition update (s : state) (id : string) (n : nat) : state :=
  fun id' => if eqb id id' then n else s id'.

Inductive aexp : Type :=
  | AVar (id : string)
  | ANum (n : nat)
  | APlus (a1 a2 : aexp)
  | AMinus (a1 a2 : aexp)
  | AMult (a1 a2 : aexp).

Inductive bexp : Type :=
  | BTrue
  | BFalse
  | BEq (a1 a2 : aexp)
  | BLe (a1 a2 : aexp)
  | BNot (b : bexp)
  | BAnd (b1 b2 : bexp).

Fixpoint aeval (a : aexp) (s : state) : nat :=
  match a with
  | ANum n => n
  | AVar id => s id
  | APlus a1 a2 => (aeval a1 s) + (aeval a2 s)
  | AMinus a1 a2 => (aeval a1 s) - (aeval a2 s)
  | AMult a1 a2 => (aeval a1 s) * (aeval a2 s)
  end.

Fixpoint beval (b : bexp) (s : state) : bool :=
  match b with
  | BTrue => true
  | BFalse => false
  | BEq a1 a2 => (aeval a1 s) =? (aeval a2 s)
  | BLe a1 a2 => (aeval a1 s) <=? (aeval a2 s)
  | BNot b1 => negb (beval b1 s)
  | BAnd b1 b2 => (beval b1 s) && (beval b2 s)
  end.

Inductive stm : Type :=
  | SSkip
  | SAsgn (id : string) (a : aexp)
  | SSeq (S1 S2 : stm)
  | SIf (c : bexp) (St Se : stm)
  | SRep (a : aexp) (Sb : stm).

(* prelude ends *)

Fixpoint iterate (n : nat) (g : state -> state) (s : state) : state :=
  match n with
  | 0 => s
  | S n' => g (iterate n' (g s))
  end.

(* iterate "g", starting from "s", "n" times *)

Fixpoint execute (S : stm) (s : state) : state :=
  match S with
  | SSkip => s
  | SAsgn x a => update s x (aeval a s)
  | SSeq S1 S2 => execute S2 (execute S1 s)
  | SIf c St Se => execute (if beval c s then St else Se) s
  | SRep a Sb => iterate (aeval a s) (execute Sb) s
(* semantics of "repeat" -- use "iterate" *)
  end.

(* test begins *)

Definition empty : state := fun _ => 0.

Definition rep : stm :=
  SSeq
    (SAsgn "x" (ANum 1))
    (SRep (ANum 6)
      (SIf (BLe (AVar "x") (ANum 3))
        (SAsgn "x" (APlus (AVar "x") (ANum 1)))
        (SAsgn "x" (APlus (AVar "x") (ANum 2))))).

Example rep_ex :
  (execute rep empty) "x"%string = 10.
Proof.
  reflexivity.
Qed.

(* test ends *)