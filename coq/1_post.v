(*
  2.1. Szintaxis
  Define the inductive type called "Tm"!
  [Tip: Ctrl+Shift+I inserts a template.]
*)

Inductive Tm : Set :=
  | num (n : nat)
  | plus (t t' : Tm)
  | isZero (t : Tm)
  | true
  | false
  | ifThenElse (t t' t'' : Tm)
(*
  | times (t t' : Tm)
  | not (t : Tm)
  | and (t t' : Tm)
*)
.


(*
  Define notations for "plus" and "ifThenElse".
*)

Notation "t + t'" := (plus t t').
Notation "'If' t 'then' t' 'else' t''" := (ifThenElse t t' t'') (at level 100).
(*
Notation "t * t'" := (times t t').
Notation "! t" := (not t) (at level 30).
Notation "t && t'" := (and t t').
*)


(*
  2.2. Feladat
  Run these commands, find the incorrect Tms and comment them out!
  [Tip: Ctrl+D inserts comments and Ctrl+Shift+D removes them.]
*)

(* a *) Check If num 3 then true else num 4.
(* b *) Check If true then true else num 4.
(* c *) Check If true then num 3 else num 4.
(* (* d *) Check If 3 then num 3 else 4. *)
(* e *) Check If (If true then true else num 2) then num 3 else true.
(* (* f *) Check 1 + (2 + true). *)
(* g *) Check num 1 + (num 2 + true).

(*
(* h *) Check If num 3 + num 4 * num 5 then ! false else true && true.
(* (* i *) Check If false && then ! else * num 3. *)
*)


(*
  2.1. Feladat
  - Extend the Tm type with "times", "not" and "and"!
  - Introduce notations for:
    + "times" (_ * _)
    + "not" (! _) [at level 30]
    + "and" (_ && _)
  - Write a few "Check" commands with example Tms using these constructors!

  - Comment out these changes.
*)


(*
  Define recursive functions (Fixpoint) on Tms
  and test them on the following test cases!
  [Tips:
    - Ctrl+Shift+F inserts a fixpoint template
    - Ctrl+Shift+m (while type name is selected) inserts a match template
  ]
*)

(*
  Defining a few test cases.
  [Tip: Ctrl+Shift+E inserts a definition template.]
*)

Definition test1 := isZero false.
Definition test2 := (num 1 + true) + true.
Definition test3 := If (If true then true else num 2) then isZero (true + num 5) else true.


(*
  2.1.1. / a
  height: Return the height of the parsed syntax tree.
*)

Fixpoint height (tm : Tm) {struct tm} : nat :=
  match tm with
   | num n => 0
   | (t + t') => 1 + max (height t) (height t')
   | isZero t => 1 + height t
   | true => 0
   | false => 0
   | (If t then t' else t'') => 1 + max (height t) (max (height t') (height t''))
  end
.

Compute (height test1). (* Should be 1. *)
Compute (height test2). (* Should be 2. *)
Compute (height test3). (* Should be 3. *)


(*
  2.1.1. / b
  trues: Return the count of "true" constructors in the parsed syntax tree.
*)

Fixpoint trues (tm : Tm) {struct tm} : nat :=
  match tm with
   | num n => 0
   | (t + t') => trues t + trues t'
   | isZero t => trues t
   | true => 1
   | false => 0
   | (If t then t' else t'') => trues t + trues t' + trues t''
  end
.

Compute (trues test1). (* Should be 0. *)
Compute (trues test2). (* Should be 2. *)
Compute (trues test3). (* Should be 4. *)


(*
  2.3. Feladat
  size: Return the count of all operators in the parsed syntax tree.
*)

Fixpoint size (tm : Tm) {struct tm} : nat :=
  match tm with
   | num n => 1
   | (t + t') => 1 + size t + size t'
   | isZero t => 1 + size t
   | true => 1
   | false => 1
   | (If t then t' else t'') => 1 + size t + size t' + size t''
  end
.

Compute (size test1). (* Should be 2. *)
Compute (size test2). (* Should be 5. *)
Compute (size test3). (* Should be 10. *)


(*
  Extra exercise:
  - Try to write a function that checks the Tms for type correctness!
*) 

Definition test4 := If isZero (num 2 + num 4) then false else true.
Definition test5 := If true then num 3 else num 5 + num 4.
Definition test6 := If num 1 then false else num 5 + true.

Inductive typecheck_result : Set :=
  | number
  | boolean
  | error
.

Fixpoint typecheck (tm : Tm) {struct tm} : typecheck_result :=
  match tm with
   | num n => number
   | plus t t' =>
     match (typecheck t), (typecheck t') with
      | number, number => number
      | _, _ => error
     end
   | isZero t =>
     match (typecheck t) with
      | number => boolean
      | _ => error
     end
   | true => boolean
   | false => boolean
   | ifThenElse t t' t'' =>
     match (typecheck t), (typecheck t'), (typecheck t'') with
      | boolean, boolean, boolean => boolean
      | boolean, number, number => number
      | _, _, _ => error
     end
  end
.

Compute typecheck test4. (* Should be boolean. *)
Compute typecheck test5. (* Should be number. *)
Compute typecheck test6. (* Should be error. *)


(* The following alternative solution is from Csaba GyĂśrgyi. *)

Definition bool_true := Coq.Init.Datatypes.true.
Definition bool_false := Coq.Init.Datatypes.false.

Fixpoint isB (tm : Tm) {struct tm} : bool :=
  match tm with
   | num n => bool_false
   | plus t t' => bool_false
   | isZero t => isN t
   | true => bool_true
   | false => bool_true
   | ifThenElse t t' t'' => isB t && isB t' && isB t''
  end
with isN (tm : Tm) {struct tm} : bool :=
  match tm with
   | num n => bool_true
   | plus t t' => isN t && isN t'
   | isZero t => bool_false
   | true => bool_false
   | false => bool_false
   | ifThenElse t t' t'' => isB t && isN t' && isN t''
  end
.

Fixpoint OK (tm : Tm) {struct tm} : bool := isN tm || isB tm.

Compute OK test4. (* Should be true. *)
Compute OK test5. (* Should be true. *)
Compute OK test6. (* Should be false. *)