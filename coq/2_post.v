Require Import Nat.
Require Import Arith.


(*
  The "Tm" type and the previously defined functions.
*)

Inductive Tm : Set :=
  | num (n : nat)
  | plus (t t' : Tm)
  | isZero (t : Tm)
  | true
  | false
  | ifThenElse (t t' t'' : Tm)
.

Notation "t + t'" := (plus t t').
Notation "'If' t 'then' t' 'else' t''" := (ifThenElse t t' t'') (at level 100).

Fixpoint height (tm : Tm) : nat :=
  match tm with
   | num n => 0
   | (t + t') => 1 + max (height t) (height t')
   | isZero t => 1 + height t
   | true => 0
   | false => 0
   | (If t then t' else t'') => 1 + max (height t) (max (height t') (height t''))
  end
.

Fixpoint trues (tm : Tm) : nat :=
  match tm with
   | num n => 0
   | (t + t') => trues t + trues t'
   | isZero t => trues t
   | true => 1
   | false => 0
   | (If t then t' else t'') => trues t + trues t' + trues t''
  end
.

Fixpoint size (tm : Tm) : nat :=
  match tm with
   | num n => 1
   | (t + t') => 1 + size t + size t'
   | isZero t => 1 + size t
   | true => 1
   | false => 1
   | (If t then t' else t'') => 1 + size t + size t' + size t''
  end
.


(*
  A few lemmas that will help prooving the later theorems.
  [Tip: Ctrl+Shift+J inserts a template for a lemma.]
*)

Lemma triple_max_1 : (forall n m o : nat, n <= max n (max m o)).
Proof.
  intros.
  exact (Nat.le_max_l _ _).
Qed.

Lemma triple_max_2 : (forall n m o : nat, m <= max n (max m o)).
Proof.
  intros.
  refine (Nat.le_trans _ (max n m) _ (Nat.le_max_r _ _) _).
  exact (Nat.max_le_compat_l _ _ _ (Nat.le_max_l _ _)).
Qed.

Lemma triple_max_3 : (forall n m o : nat, o <= max n (max m o)).
Proof.
  intros.
  refine (Nat.le_trans _ (max n o) _ (Nat.le_max_r _ _) _).
  exact (Nat.max_le_compat_l _ _ _ (Nat.le_max_r _ _)).
Qed.

Lemma max_le_plus : (forall n m : nat, max n m <= n + m).
Proof.
  intros.
  refine (Nat.max_case n m (fun x => x <= n + m) _ _).
  - exact (le_plus_l _ _).
  - exact (le_plus_r _ _).
Qed.

Lemma max_le_plus_3 : (forall n m o : nat, max n (max m o) <= n + m + o).
Proof.
  intros.
  refine (Nat.max_case _ _ (fun x => x <= n + m + o) _ _).
  - rewrite plus_assoc_reverse.
    exact (le_plus_l _ _).
  - refine (le_trans _ (m + o) _ (max_le_plus _ _) _).
    + rewrite plus_assoc_reverse.
      exact (le_plus_r _ _).
Qed.

(*
  Proove the theorem about the connection of trues and height from page 10.
  (P(t) = trues(t) <= 3 ^ height(t))
  [Tip: Ctrl+Shift+T inserts a template for a theorem.]
*)

Theorem trues_height : (forall t : Tm, trues t <= pow 3 (height t)).
Proof.
  induction t.

  (* num n *)
  - simpl. exact (Nat.le_0_l _).

  (* t + t' *)
  - simpl. rewrite (plus_comm _ 0). simpl.
    refine (le_trans _ (3 ^ height t1 + 3 ^ height t2) _ _ _).
    + exact (Nat.add_le_mono _ _ _ _ IHt1 IHt2).
    + rewrite <- plus_assoc_reverse.
      refine (le_plus_trans _ _ _ _).
      * refine (Nat.add_le_mono _ _ _ _ _ _).
        -- refine (Nat.pow_le_mono_r _ _ _ _ _). auto. exact (Nat.le_max_l _ _).
        -- refine (Nat.pow_le_mono_r _ _ _ _ _). auto. exact (Nat.le_max_r _ _).

  (* isZero t *)
  - simpl. exact (le_plus_trans _ _ _ IHt).

  (* true *)
  - simpl. exact (Nat.le_refl _).

  (* false *)
  - simpl. exact (Nat.le_0_1).

  (* If t then t' else t'' *)
  - simpl. rewrite (plus_comm _ 0). simpl.
    refine (le_trans _ (3 ^ height t1 + 3 ^ height t2 + 3 ^ height t3) _ _ _).
    refine (Nat.add_le_mono _ _ _ _ (Nat.add_le_mono _ _ _ _ IHt1 IHt2) IHt3).
    + rewrite plus_assoc_reverse.
      refine (Nat.add_le_mono _ _ _ _ _ (Nat.add_le_mono _ _ _ _ _ _)).
      -- refine (Nat.pow_le_mono_r _ _ _ _ (triple_max_1 _ _ _)). auto.
      -- refine (Nat.pow_le_mono_r _ _ _ _ (triple_max_2 _ _ _)). auto.
      -- refine (Nat.pow_le_mono_r _ _ _ _ (triple_max_3 _ _ _)). auto.
Qed.


(*
  2.4. Feladat
  Proove the theorem about the connection of height and size from the bottom of page 11.
  (P(t) = height(t) < size(t))
  [Tip: Ctrl+Shift+T]
*)

Theorem height_size : (forall t : Tm, height t < size t).
Proof.
  induction t.

  (* num n *)
  - simpl. exact (Nat.lt_0_succ _).

  (* t + t' *)
  - simpl.
    refine (lt_n_S _ _ _).
    + refine (le_lt_trans _ (height t1 + height t2) _ _ _).
      * exact (max_le_plus _ _).
      * refine (Nat.add_lt_mono _ _ _ _ IHt1 IHt2).

  (* isZero t *)
  - simpl. exact (lt_n_S _ _ IHt).

  (* true *)
  - simpl. exact (Nat.lt_0_succ _).

  (* false *)
  - simpl. exact (Nat.lt_0_succ _).

  (* If t then t' else t'' *)
  - simpl.
    refine (lt_n_S _ _ _).
    + refine (le_lt_trans _ (height t1 + height t2 + height t3) _ _ _).
      * refine (max_le_plus_3 _ _ _).
      * refine (Nat.add_lt_mono _ _ _ _ (Nat.add_lt_mono _ _ _ _ IHt1 IHt2) IHt3).
Qed.


(*
  2.2. TĂ­pusrendszer
  Define the inductive type of "Ty"!
  [Tip: Ctrl+Shift+I]
*)

Inductive Ty : Set := Nat | Bool.


(*
  Define the typing relation "::"!
  [Tip: Ctrl+Shift+I]
*)

Inductive Judgement : Tm -> Ty -> Prop :=
  | J_num {n : nat} : (num n) :: Nat
  | J_plus {t t' : Tm} (j : t :: Nat) (j' : t' :: Nat) : (t + t') :: Nat
  | J_isZero {t : Tm} (j : t :: Nat) : (isZero t) :: Bool
  | J_true : true :: Bool
  | J_false : false :: Bool

  | J_ifThenElse
    {t t' t'' : Tm} {A : Ty}
    (j : t :: Bool) (j' : t' :: A) (j'' : t'' :: A)
    : (If t then t' else t'') :: A

where "tm :: ty" := (Judgement tm ty).


(*
  Try to prove the typing of the following terms!
  Comment out the impossible ones!
  [Tip: Ctrl+D / Ctrl+Shift+D]
*)

Lemma JudgementTest : isZero (num 1 + num 2) :: Bool.
Proof.
  refine (J_isZero _).
  - refine (J_plus _ _).
    + refine (J_num).
    + refine (J_num).
Qed.

Lemma F_2_7_a : isZero(num 0 + num 1) :: Bool.
Proof.
  refine (J_isZero _).
  refine (J_plus _ _).
  - refine (J_num).
  - refine (J_num).
Qed.

Lemma F_2_7_b : (If true then false else false) :: Bool.
Proof.
  refine (J_ifThenElse _ _ _).
  - refine (J_true).
  - refine (J_false).
  - refine (J_false).
Qed.

Lemma F_2_7_c : (num 0 + If false then num 0 else num 2) :: Nat.
Proof.
  refine (J_plus _ _).
  - refine (J_num).
  - refine (J_ifThenElse _ _ _).
    + refine (J_false).
    + refine (J_num).
    + refine (J_num).
Qed.

(*
Lemma F_2_7_d : (num 0 + If false then true else true) :: Bool.
Proof.

Qed.
*)

(*
Lemma F_2_7_e : (If isZero (num 0 + num 1) then false else num 2) :: Nat.
Proof.

Qed.
*)

(*
Lemma F_2_7_f : (If isZero (num 0 + num 0) then false else num 2) :: Bool.
 Proof.

Qed.
*)