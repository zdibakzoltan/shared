Inductive aexp : Type :=
| ALit (n : nat)
| APlus (a1 a2 : aexp)
| AMinus (a1 a2 : aexp)
| AMult (a1 a2 : aexp).

Fixpoint aeval (a: aexp) : nat :=
match a with
| ALit n => n
| APlus a1 a2 => aeval a1 + aeval a2
| AMinus a1 a2 => aeval a1 - aeval a2
| AMult a1 a2 => aeval a1 * aeval a2
end.

Fixpoint opt_0plus (a: aexp) : aexp :=
match a with
| ALit n => ALit n
| APlus (ALit 0) a2 => opt_0plus a2
| APlus a1 a2 => APlus (opt_0plus a1) (opt_0plus a2)
| AMinus a1 a2 => AMinus (opt_0plus a1) (opt_0plus a2)
| AMult a1 a2 => AMult (opt_0plus a1) (opt_0plus a2)
end.

Definition exp1 := APlus (ALit 0) (APlus (ALit 0) (ALit 5)).

Example test_opt_0plus_1:
  opt_0plus exp1 = ALit 5.
Proof.
  auto.
Qed.

Theorem opt_0plus_sound:
  forall a: aexp, aeval a = aeval (opt_0plus a).
Proof.
  intros. induction a.
  (* a = ALit n *)
  simpl. reflexivity.
  (* a = APlus a1 a2 *)
  destruct a1.
    (* a1 = ALit n *)
    destruct n.
      (* n = 0 *)
      simpl.
      apply IHa2.
      (* n = S n *)
      simpl.
      rewrite IHa2.
      trivial. (* reflexivity *)

    
    (* a1 = APlus a1_1 a1_2 first solution*)
    simpl in IHa1.
    simpl.  (* simpl in * *)
    rewrite IHa1.
    rewrite IHa2.
    reflexivity.

    (* a1 = APlus a1_1 a1_2 second solution*)
    remember (APlus a1_1 a1_2) as a1.
    simpl.
    rewrite IHa1.
    rewrite IHa2.
    rewrite Heqa1.
    simpl.
    reflexivity.

    (* a1 = APlus a1_1 a1_2 third solution *)
    simpl.
    auto.

    (* a1 = APlus a1_1 a1_2 fourth solution *)
    simpl.
    simpl in *.
    rewrite IHa1.
    rewrite IHa2.
    reflexivity.
    (* a1 = AMinus a1_1 a1_2 *)
    simpl.
    simpl in *.
    rewrite IHa1.
    rewrite IHa2.
    reflexivity.
    (* a1 = AMult a1_1 a1_2 *)
    remember (APlus a1_1 a1_2) as a1.
    simpl.
    simpl in *.
    rewrite IHa1.
    rewrite IHa2.
    reflexivity.

  (* a = AMinus a1 a2 *)
  simpl.
  rewrite IHa1.
  rewrite IHa2.
  reflexivity.
  (* a = AMult a1 a2 *)
  simpl.
  rewrite IHa1.
  rewrite IHa2.
  reflexivity.
Qed.




-----------------------------------------



Inductive aexp : Type :=
| ALit (n : nat)
| APlus (a1 a2 : aexp)
| AMinus (a1 a2 : aexp)
| AMult (a1 a2 : aexp).

Fixpoint aeval (a: aexp) : nat :=
match a with
| ALit n => n
| APlus a1 a2 => aeval a1 + aeval a2
| AMinus a1 a2 => aeval a1 - aeval a2
| AMult a1 a2 => aeval a1 * aeval a2
end.

Fixpoint opt_0plus (a: aexp) : aexp :=
match a with
| ALit n => ALit n
| APlus (ALit 0) a2 => opt_0plus a2
| APlus a1 a2 => APlus (opt_0plus a1) (opt_0plus a2)
| AMinus a1 a2 => AMinus (opt_0plus a1) (opt_0plus a2)
| AMult a1 a2 => AMult (opt_0plus a1) (opt_0plus a2)
end.

Definition exp1 := APlus (ALit 0) (APlus (ALit 0) (ALit 5)).

Example test_opt_0plus_1:
  opt_0plus exp1 = ALit 5.
Proof.
  auto.
Qed.

Theorem opt_0plus_sound:
  forall a: aexp, aeval a = aeval (opt_0plus a).
Proof.
  induction a;

  (* a = AMinus a1 a2 *)
  (* a = AMult a1 a2 *)
  try(simpl;
     rewrite IHa1;
     rewrite IHa2;
     reflexivity).

  (* a = ALit n *)
  simpl. reflexivity.
  (* a = APlus a1 a2 *)
  destruct a1;
   (* a1 = APlus a1_1 a1_2 *)
   (* a1 = AMinus a1_1 a1_2 *)
   (* a1 = AMult a1_1 a1_2 *)
   try(simpl;
       simpl in *;
       rewrite IHa1;
       rewrite IHa2;
       reflexivity).
    (* a1 = ALit n *)
    destruct n;
      simpl;
      rewrite IHa2;
      reflexivity. 
Qed.












