(* BEGIN FIX *)
Inductive aexp : Type :=
| ALit (n : nat)
| APlus (a1 a2 : aexp)
| AMinus (a1 a2 : aexp).

Fixpoint leaves_cnt (a: aexp) : nat :=
match a with
| ALit n => 1
| APlus a1 a2 => leaves_cnt a1 + leaves_cnt a2
| AMinus a1 a2 => leaves_cnt a1 + leaves_cnt a2
end.

Require Import Coq.Arith.Plus. (* Lemma plus_comm *)

Lemma plus_succ : forall n, n + 1 = S n.
Proof.
(* END FIX *)
intros n. induction n. simpl. reflexivity.
simpl. rewrite IHn. reflexivity.
Qed.

(* BEGIN FIX *)
Lemma leaves_plus :
  forall a : aexp, leaves_cnt (APlus (ALit 1) a) = leaves_cnt (APlus a (ALit 1)).
Proof.
(* END FIX *)
intros. induction a. simpl. reflexivity. 
simpl. rewrite plus_succ. reflexivity.
simpl. rewrite plus_succ. reflexivity.
Qed.

(* BEGIN FIX *)
Require Import Coq.Arith.Max. (* max *)
Fixpoint height (a: aexp) : nat :=
(* END FIX *)
match a with
| ALit n => 0
| APlus a1 a2 => S (max (height a1) (height a2))
| AMinus a1 a2 => S (max (height a1) (height a2))
end. 

(* BEGIN FIX *)
Example height1: height (ALit 42) = 0.
(* END FIX *)
Proof.
simpl. reflexivity.
Qed.

(* BEGIN FIX *)
Example height2: height (APlus (APlus (ALit 1) (ALit 2)) (ALit 3)) = 2.
(* END FIX *)
Proof.
simpl. reflexivity.
Qed.