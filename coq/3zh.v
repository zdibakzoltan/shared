Inductive Nat : Type := O | S (n:Nat).

Fixpoint plus ( n m : Nat ) {struct n} : Nat :=
  match n with O => m | S n' => S (plus n' m) end.

Notation "n + m" := (plus n m) (at level 50, left associativity).

Lemma plus_lid :
  forall n : Nat, O + n = n.
Proof.
  intro.
  simpl.
  reflexivity.
Qed.

Lemma plus_rid :
  forall n : Nat, n + O = n.
Proof.
  intro.
  induction n as [|n'].
    (* n = O *)  (* would also work: *)
    simpl.       (* apply plusn_lid. *)
    reflexivity.
    (* n = S n', n' + O = n' *)
    simpl.         (* would also work: *)
    rewrite IHn'.  (* apply S_cong.    *)
    reflexivity.   (* apply IHn'.      *)
Qed.

Lemma plus_lS :
  forall n m : Nat, S n + m = S (n + m).
Proof.
  simpl.
  reflexivity.
Qed.

Lemma plus_rS :
  forall n m : Nat, n + S m = S (n + m).
Proof.
  intros.
  induction n as [|n'].
    (* n = O *)  (* would also work: *)
    simpl.       (* apply plusn_lid. *)
    reflexivity.
    (* n = S n', n' + S m = S (n' + m) *)
    simpl.         (* would also work: *)
    rewrite IHn'.  (* apply S_cong.    *)
    reflexivity.   (* apply IHn'.      *)
Qed.

Lemma comm_plus: forall n m: Nat, n + m = m + n.
Proof.
  intros. induction n. simpl. symmetry. apply plus_rid.
  rewrite plus_rS. simpl. rewrite IHn. reflexivity.
Qed.