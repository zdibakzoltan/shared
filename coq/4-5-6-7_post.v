Require Import Nat.
Require Import Arith.


(*
  The "Tm" type and the previously defined typing relation.
*)

Inductive Tm : Set :=
  | num (n : nat)
  | plus (t t' : Tm)
  | isZero (t : Tm)
  | true
  | false
  | ifThenElse (t t' t'' : Tm)
.


Notation "t + t'" := (plus t t') : term_scope.
Notation
  "'If' t 'then' t' 'else' t''" :=
  (ifThenElse t t' t'')
  (at level 100)
  : term_scope
.

(* Delimit Scope term_scope with term. *)
(* Bind Scope term_scope with Tm. *)
Open Scope term_scope.

(* Compute (plus (num 1) (num 2)). *)
Compute (num 1 + num (2 + 3)).
Compute (2 + 4)%nat.

(* The set of Ty and the inductive typing relation. *)

Inductive Ty : Set := Nat | Bool.

Inductive TypeJudgement : Tm -> Ty -> Prop :=
  | TJ_num {n : nat} : (num n) :: Nat
  | TJ_plus {t t' : Tm} (j : t :: Nat) (j' : t' :: Nat) : (t + t') :: Nat
  | TJ_isZero {t : Tm} (j : t :: Nat) : (isZero t) :: Bool
  | TJ_true : true :: Bool
  | TJ_false : false :: Bool

  | TJ_ifThenElse
    {t t' t'' : Tm} {A : Ty}
    (j : t :: Bool) (j' : t' :: A) (j'' : t'' :: A)
    : (If t then t' else t'') :: A

where "tm :: ty" := (TypeJudgement tm ty) : term_scope.


(*
  2.3. OperĂĄciĂłs szemantika
*)

(* val : 2.9. - 2.11. *)

Reserved Notation "t 'val'" (at level 1).

Inductive ValueJudgement : Tm -> Prop :=
  | VJ_num {n : nat} : (num n) val
  | VJ_true : true val
  | VJ_false : false val
where
  "t 'val'" :=
  (ValueJudgement t)
  : term_scope.


(*
  Transition judgements.
*)

Reserved Notation "t |-> t'" (at level 100).

(* One Step Transition : 2.14. - 2.22. *)

Inductive OneStepTransitionJudgement : Tm -> Tm -> Prop :=
  | OSTJ_sum {n1 n2 n : nat} :
    ((n1 + n2)%nat = n) ->
    num n1 + num n2 |-> num n

  | OSTJ_isZero_true :
    (isZero (num 0)) |-> true

  | OSTJ_isZero_false {n : nat} :
    (n > 0) ->
    isZero (num n) |-> false

  | OSTJ_ifThenElse_true {t t' : Tm} :
    If true then t else t' |-> t

  | OSTJ_ifThenElse_false {t t' : Tm} :
    If false then t else t' |-> t'

  | OSTJ_plus_left {t1 t1' t2 : Tm} :
    (t1 |-> t1') ->
    t1 + t2 |-> t1' + t2

  | OSTJ_plus_right {t1 t2 t2' : Tm} :
    t1 val -> (t2 |-> t2') ->
    t1 + t2 |-> t1 + t2'

  | OSTJ_isZero {t t' : Tm} :
    (t |-> t') ->
    isZero t |-> isZero t'

  | OSTJ_ifThenElse {t t' t1 t2 : Tm} :
    (t |-> t') ->
    If t then t1 else t2 |-> If t' then t1 else t2
where
  "t |-> t'" :=
  (OneStepTransitionJudgement t t')
  : term_scope.


(* Any Step Transition : 2.12. - 2.13. *)

Reserved Notation "t |->* t'" (at level 100).

Inductive AnyStepTransitionJudgement : Tm -> Tm -> Prop :=
  | ASTJ_refl {t : Tm} :
    t |->* t
  | ASTJ_trans {t t'' : Tm} (t' : Tm) : 
    (t |-> t') -> (t' |->* t'') ->
    t |->* t''
where "t |->* t'" := (AnyStepTransitionJudgement t t') : term_scope.


Lemma p :
  (num 1 + num 2) + (num 3 + num 4) |-> num 3 + (num 3 + num 4)
.
Proof.
  apply OSTJ_plus_left.
  apply OSTJ_sum.
  simpl. reflexivity.
Qed.

Lemma q :
  num 3 + (num 3 + num 4) |-> num 3 + num 7
.
Proof.
  apply OSTJ_plus_right.
  - apply VJ_num.
  - apply OSTJ_sum.
    simpl. reflexivity.
Qed.

Lemma r :
  num 3 + num 7 |-> num 10
.
Proof.
  apply OSTJ_sum.
  simpl. reflexivity.
Qed.

Lemma TransitionTest1 :
  (num 1 + num 2) + (num 3 + num 4) |->* num 10
.
Proof.
  refine (ASTJ_trans _ p _).
  - refine (ASTJ_trans _ q _).
    + refine (ASTJ_trans _ r _).
      * exact ASTJ_refl.
Qed.


Lemma TransitionTest2 :
  If isZero (num 1 + num 2) then num 1 else (num 3 + num 1) |->* num 4
.
Proof.
  eapply ASTJ_trans.
  {
    eapply OSTJ_ifThenElse.
    eapply OSTJ_isZero.
    eapply OSTJ_sum; reflexivity.
  }

  eapply ASTJ_trans.
  {
    eapply OSTJ_ifThenElse.
    eapply OSTJ_isZero_false.
    eapply gt_Sn_O.
  }

  eapply ASTJ_trans.
  {
    eapply OSTJ_ifThenElse_false.
  }

  eapply ASTJ_trans.
  {
    eapply OSTJ_sum; reflexivity.
  }

  eapply ASTJ_refl.
Qed.

(* Proof.
  refine (ASTJ_trans _ _ _).
  - refine (OSTJ_ifThenElse _).
    + refine (OSTJ_isZero _).
      * refine (OSTJ_sum _).
        -- simpl. reflexivity.
  - refine (ASTJ_trans _ _ _).
    + refine (OSTJ_ifThenElse _).
      * refine (OSTJ_isZero_false _).
        -- auto.
    + refine (ASTJ_trans _ _ _).
      * refine (OSTJ_ifThenElse_false).
      * refine (ASTJ_trans _ _ _).
        -- refine (OSTJ_sum _).
           simpl. reflexivity.
        -- exact ASTJ_refl.
Qed. *)

Ltac transition_solver :=
  repeat (
    (eapply ASTJ_refl)
    ||
    (eapply ASTJ_trans; repeat (
      (eapply OSTJ_sum; reflexivity) ||
      (eapply OSTJ_plus_right; eapply VJ_num) ||
      (eapply OSTJ_plus_left) ||
      (eapply OSTJ_isZero_true) ||
      (eapply OSTJ_isZero_false; eapply gt_Sn_O) ||
      (eapply OSTJ_isZero) ||
      (eapply OSTJ_ifThenElse_true) ||
      (eapply OSTJ_ifThenElse_false) ||
      (eapply OSTJ_ifThenElse)
    ))
  )
.

(*
  2.19. Lemma.
  No transition from val.
*)

Lemma no_val_transition :
  forall t t' : Tm, ~ ((t val) /\ (t |-> t'))
.
Proof.
  unfold not. intros. inversion H.
  induction H1; inversion H0.
Qed.


(*
  2.20. Lemma.
  Determinism.
*)

Lemma determinism {t t'} :
  (t |-> t') -> (forall t'' : Tm, (t |-> t'') -> (t' = t''))
.
Proof.
  intros H.
  induction H.

  (* sum *)
  - intros. inversion H0.
    + rewrite <- H. rewrite <- H4. reflexivity.
    + inversion H4.
    + inversion H5.

  (* isZero_true *)
  - intros. inversion H.
    + reflexivity.
    + inversion H1.
    + inversion H1.

  (* isZero_false *)
  - intros. inversion H0.
    + rewrite <- H2 in H. inversion H.
    + reflexivity.
    + inversion H2.

  (* ifThenElse_true *)
  - intros. inversion H.
    + reflexivity.
    + inversion H4.

  (* ifThenElse_false *)
  - intros. inversion H.
    + reflexivity.
    + inversion H4.

  (* plus_left *)
  - intros. inversion H0.
    + rewrite <- H1 in *. inversion H.
    + pose (ir := IHOneStepTransitionJudgement t1'0 H4).
      rewrite ir. reflexivity.
    + pose (nvt := no_val_transition t1 t1').
      unfold not in nvt.
      pose (f := nvt (conj H3 H)). inversion f.

  (* plus_right *)
  - intros. inversion H1.
    + rewrite <- H4 in *. inversion H0.
    + pose (nvt := no_val_transition t1 t1').
      unfold not in nvt.
      pose (f := nvt (conj H H5)). inversion f.
    + pose (ir := IHOneStepTransitionJudgement t2'0 H6).
      rewrite ir. reflexivity.

  (* isZero *)
  - intros. inversion H0.
    + rewrite <- H2 in *. inversion H.
    + rewrite <- H1 in *. inversion H.
    + pose (ir := IHOneStepTransitionJudgement t'0 H2).
      rewrite ir. reflexivity.

  (* ifThenElse *)
  - intros. inversion H0.
    + rewrite <- H2 in *. inversion H.
    + rewrite <- H2 in *. inversion H.
    + pose (ir := IHOneStepTransitionJudgement t'0 H5).
      rewrite ir. reflexivity.
Qed.



(*
  2.27. TĂŠtel.
  Theorem of type preservation.
*)

Theorem type_preservation {t t' : Tm} :
  (t |-> t') -> (forall A : Ty, (t :: A) -> (t' :: A))
.
Proof.
  intros H.
  induction H.

  (* sum *)
  - intros. inversion H0. exact TJ_num.

  (* isZero_true *)
  - intros. inversion H. exact TJ_true.

  (* isZero_false *)
  - intros. inversion H0. exact TJ_false.

  (* ifThenElse_true *)
  - intros. inversion H. exact j'.

  (* ifThenElse_false *)
  - intros. inversion H. exact j''.

  (* plus_left *)
  - intros. inversion H0.
    refine (TJ_plus _ j').
    + pose (ir := IHOneStepTransitionJudgement Nat j).
      exact ir.

  (* plus_right *)
  - intros. inversion H1.
    refine (TJ_plus j _).
    + pose (ir := IHOneStepTransitionJudgement Nat j').
      exact ir.

  (* isZero *)
  - intros. inversion H0.
    refine (TJ_isZero _).
    + exact (IHOneStepTransitionJudgement Nat j).

  (* ifThenElse *)
  - intros. inversion H0.
    refine (TJ_ifThenElse _ j' j'').
    + exact (IHOneStepTransitionJudgement Bool j).
Qed.


(*
  2.29. Lemma
*)
Lemma progress_helper_Nat {t : Tm} :
  (t :: Nat) -> (t val) -> (exists n : nat, t = num n)
.
Proof.
  intros. inversion H0.
  + refine (ex_intro _ _ _). reflexivity.
  + rewrite <- H1 in *. inversion H.
  + rewrite <- H1 in *. inversion H.
Qed.

Lemma progress_helper_Bool {t : Tm} :
  (t :: Bool) -> (t val) -> (t = true \/ t = false)
.
Proof.
  intros. inversion H0.
  + rewrite <- H1 in *. inversion H.
  + refine (or_introl _). reflexivity.
  + refine (or_intror _). reflexivity.
Qed.

Lemma transition_no_val : (forall t t' : Tm, (t |-> t') -> ~ (t val)).
Proof.
  unfold not.
  intros.
  inversion H0; subst.
  - inversion H. 
  - inversion H.
  - inversion H.
Qed.

(*
  2.28. TĂŠtel.
  Theorem of progress.
*)
Theorem progress {t : Tm} {A : Ty} :
  (t :: A) -> t val \/ (exists t' : Tm, t |-> t')
.
Proof.
  intros. induction H.

  (* num *)
  - apply or_introl. exact VJ_num.

  (* plus *)
  - case IHTypeJudgement1.
    + case IHTypeJudgement2.
      * intros.
        pose (n1 := progress_helper_Nat H H2).
        inversion n1. rewrite H3.

        pose (n2 := progress_helper_Nat H0 H1).
        inversion n2. rewrite H4.

        apply or_intror.
        refine (ex_intro _ _ _).
        -- refine (OSTJ_sum _). reflexivity.

      * intros.
        inversion H1.
        apply or_intror.
        refine (ex_intro _ _ _).
        -- exact (OSTJ_plus_right H2 H3).

    + intros.
      inversion H1.
      apply or_intror.
      refine (ex_intro _ _ _).
      -- exact (OSTJ_plus_left H2).

  (* isZero *)
  - apply or_intror.
    inversion IHTypeJudgement.
    + pose (n := progress_helper_Nat H H0).
      inversion n. rewrite H1.
      case x.
      * refine (ex_intro _ _ _).
        exact OSTJ_isZero_true.

      * intros.
        refine (ex_intro _ _ _).
        refine (OSTJ_isZero_false _).
        exact (gt_Sn_O _).

    + inversion H0.
      refine (ex_intro _ _ _).
      exact (OSTJ_isZero H1).

  (* true *)
  - apply or_introl. exact VJ_true.

  (* false *)
  - apply or_introl. exact VJ_false.

  (* ifThenElse *)
  - apply or_intror.
    case IHTypeJudgement1.
    + intros.
      case (progress_helper_Bool H H2).
      * intros. rewrite H3.
        refine (ex_intro _ _ _).
        apply OSTJ_ifThenElse_true.
      * intros. rewrite H3.
        refine (ex_intro _ _ _).
        apply OSTJ_ifThenElse_false.
    + intros.
      inversion H2.
      refine (ex_intro _ _ _).
      exact (OSTJ_ifThenElse H3).
Qed.


(* Composability *)

Theorem composability {t t' t'' : Tm} :
  (* forall t t' t'' : Tm, *)
  (t |->* t') -> (t' |->* t'') -> (t |->* t'')
.
Proof.
  intros.
  induction H as [ | a b c].
  - assumption.
  - eapply ASTJ_trans.
    + exact H.
    + exact (IHAnyStepTransitionJudgement H0).
Qed.


(* Not backwards type preservation. *)

Theorem not_backwards_type_preservation :
  ~ (
      forall t t', (t |-> t') ->
      forall A : Ty, (t' :: A) -> (t :: A)
    )
.
Proof.
  unfold not.
  intros.
  pose (H1 := H (If true then num 1 else false) (num 1)).
  pose (H2 := H1 OSTJ_ifThenElse_true Nat TJ_num).
  inversion H2.
  inversion j''.
Qed.


(* Any Step Type Preservation *)

Theorem any_step_type_preservation {t t' : Tm} :
  (t |->* t') -> (forall A : Ty, (t :: A) -> (t' :: A))
.
Proof.
  intros.
  induction H.
  - exact H0.
  - eapply IHAnyStepTransitionJudgement.
    eapply type_preservation.
    + exact H.
    + exact H0.
Qed.