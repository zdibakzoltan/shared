Require Import Coq.Arith.EqNat.

Example nateq: beq_nat 1 2 = false.
simpl. reflexivity.
Qed.

Require Import Coq.Arith.Compare_dec.
Example natle: leb 1 2 = true.
simpl. reflexivity.
Qed.

(* BEGIN FIX *)
Inductive bexp : Type :=
  | BTrue
  | BFalse
  | BEq (n1 n2 : nat)
  | BGe (n1 n2 : nat)
  | BNot (b : bexp)
  | BOr (b1 b2 : bexp).


Fixpoint beval (b : bexp) : bool := 
(* END FIX *)
match b with
| BTrue => true
| BFalse => false
| BEq b1 b2 => beq_nat b1 b2
| BGe b1 b2 => leb b2 b1
| BNot b1 => negb (beval b1)
| BOr b1 b2 => orb (beval b1) (beval b2)
end.

(* BEGIN FIX *)
Example beval_test_1 : beval (BGe 3 4) = false.
(* END FIX *)
simpl. reflexivity.
Qed.

(* BEGIN FIX *)
Example beval_test_2 : beval (BGe 3 3) = true.
(* END FIX *)
simpl. reflexivity.
Qed.

(* BEGIN FIX *)
Example beval_test_3 : beval (BGe 5 3) = true.
(* END FIX *)
simpl. reflexivity.
Qed.

(* BEGIN FIX *)
Example beval_test_4 : beval (BOr (BGe 3 4) (BGe 3 2)) = true.
(* END FIX *)
simpl. reflexivity.
Qed.

(* BEGIN FIX *)
Definition BAnd (b1 b2 : bexp) : bexp := 
(* END FIX *)
match (andb (beval b1) (beval b2)) with
| true => BTrue
| false => BFalse
end.

(* BEGIN FIX *)
Example beval_test_5 : beval (BAnd (BGe 3 4) (BGe 3 2)) = false.
(* END FIX *)
simpl. reflexivity.
Qed.

(* BEGIN FIX *)
Example beval_test_6 : beval (BAnd (BGe 4 4) (BGe 3 2)) = true.
(* END FIX *)
simpl. reflexivity.
Qed.

(* BEGIN FIX *)
Example beval_test_7 : beval
  (BAnd
    (BOr
      (BOr
        (BNot BTrue)
        (BEq 3 3))
      (BGe 5 3))
    (BNot (BEq 3 4)))
  = true.
(* END FIX *)
simpl. reflexivity.
Qed.

(* BEGIN FIX *)
Lemma bor_left_unit (b : bexp) : beval (BOr BFalse b) = beval b.
(* END FIX *)
simpl. reflexivity.
Qed.

(* BEGIN FIX *)
Lemma lem (b : bexp)(p : beval b = true) : beval (BAnd b BTrue) = true.
(* END FIX *)
intros. induction b. simpl. reflexivity. auto. unfold BAnd. rewrite p. simpl. reflexivity.
unfold BAnd. rewrite p. simpl. reflexivity. unfold BAnd. rewrite p. simpl. reflexivity.
unfold BAnd. rewrite p. simpl. reflexivity. 
Qed.