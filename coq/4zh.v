Inductive aexp : Type :=
| ALit (n : nat)
| APlus (a1 a2 : aexp)
| AMinus (a1 a2 : aexp)
| AMult (a1 a2 : aexp).

Fixpoint aeval (a: aexp) : nat :=
match a with
| ALit n => n
| APlus a1 a2 => aeval a1 + aeval a2
| AMinus a1 a2 => aeval a1 - aeval a2
| AMult a1 a2 => aeval a1 * aeval a2
end.

Fixpoint leaves_cnt (a: aexp) : nat :=
match a with
| ALit n => 1
| APlus a1 a2 => leaves_cnt a1 + leaves_cnt a2
| AMinus a1 a2 => leaves_cnt a1 + leaves_cnt a2
| AMult a1 a2 => leaves_cnt a1 + leaves_cnt a2
end.

Require Import Coq.Arith.Plus. (* plus_comm *)

Lemma plus_succ:
  forall n, n + 1 = S n.
Proof.
  intros.
  rewrite plus_comm. (* n + 1 = 1 + n *)
  reflexivity...
Qed.

Lemma leaves_plus:
  forall a : aexp, leaves_cnt (APlus (ALit 1) a) = leaves_cnt (APlus a (ALit 1)).
Proof.
  intros.
  destruct a.
   (* a = ALit a1 *)
    reflexivity...
   (* a = APlus a1 a2 *)
    simpl. rewrite <- plus_succ.
    reflexivity...
   (* a = AMinus a1 a2 *)
    simpl. rewrite <- plus_succ.
    reflexivity...
   (* a = aMult a1 a2 *)
   simpl. rewrite <- plus_succ.
    reflexivity...
Qed.

Require Import Coq.Arith.Max. (* max *)

Fixpoint height (a: aexp) : nat :=
match a with
| ALit n => 0
| APlus a1 a2 => 1 + max (height a1) (height a2)
| AMinus a1 a2 => 1 + max (height a1) (height a2)
| AMult a1 a2 => 1 + max (height a1) (height a2)
end.

Fixpoint nodes_cnt (a: aexp) : nat :=
match a with
| ALit n => 1
| APlus a1 a2 => nodes_cnt a1 + nodes_cnt a2 +1
| AMinus a1 a2 =>  nodes_cnt a1 + nodes_cnt a2 +1
| AMult a1 a2 =>  nodes_cnt a1 + nodes_cnt a2 +1
end.

Example test_nodes_cnt1 : nodes_cnt (ALit 1) = 1.
Proof.
intros. simpl. reflexivity.
Qed.

Example test_nodes_cnt2 : nodes_cnt (APlus (ALit 1) (ALit 1)) = 3.
Proof.
intros. simpl. reflexivity.
Qed.