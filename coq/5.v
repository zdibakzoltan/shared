Require Import Coq.Strings.String.
Definition ident : Type := string.
Inductive aexp : Type :=
| ALit (n : nat)
| AVar (x : ident)
| APlus (a1 a2 : aexp)
| AMinus (a1 a2 : aexp)
| AMult (a1 a2 : aexp).

Definition X: ident := "X"%string.
Definition Y: ident := "Y"%string.
Definition Z: ident := "Z"%string.

Definition varexp: aexp := APlus (AVar X) (AVar Y).
Definition state : Type := ident -> nat.

Fixpoint aeval (a: aexp)(s: state) : nat :=
match a with
| ALit n => n
| AVar x => s x
| APlus a1 a2 => aeval a1 s + aeval a2 s
| AMinus a1 a2 => aeval a1 s - aeval a2 s
| AMult a1 a2 => aeval a1 s * aeval a2 s
end.

Definition aState : state :=
  fun x =>
    match x with
    | "X"%string => 2
    | "Y"%string => 3
    | _ => 0 (* default value for all (global) variables *)
    end.

Eval compute in aeval varexp aState.
Eval compute in aeval (ALit 42) aState.

Definition update (s: state) (x: ident) (n: nat): state :=
  fun y =>
    if eqb x y then n else s y.

Definition anotherState: state :=
  update aState X 11.

Eval compute in aeval (ALit 42) anotherState.
Eval compute in aeval varexp anotherState.

Lemma update_sound:
  forall s: state, forall x: ident, forall n: nat,
    (update s x n) x = n.
Proof.
  intros. unfold update. rewrite eqb_refl. reflexivity.
Qed.

Lemma update_onlyx:
  forall s:state, forall x x': ident, forall n:nat,
    ~(x = x') -> (update s x n) x' = s x'.
Proof.
  intros. unfold update. apply eqb_neq in H. rewrite eqb_refl. reflexivity.
Qed.