Require Import Coq.Strings.String.
Definition ident : Type := string.
Inductive aexp : Type :=
| ALit (n : nat)
| AVar (x : ident)
| APlus (a1 a2 : aexp)
| AMinus (a1 a2 : aexp)
| AMult (a1 a2 : aexp).

Definition state : Type := ident -> nat.
Fixpoint aeval (a: aexp)(s: state) : nat :=
match a with
| ALit n => n
| AVar x => s x
| APlus a1 a2 => aeval a1 s + aeval a2 s
| AMinus a1 a2 => aeval a1 s - aeval a2 s
| AMult a1 a2 => aeval a1 s * aeval a2 s
end.

Definition aState : state :=
  fun x =>
    match x with
    | "X"%string => 2
    | "Y"%string => 3
    | _ => 0 (* default value for all (global) variables *)
    end.

Fixpoint const_prop(a:aexp)(s:state): aexp := 
match a with
| ALit n => ALit n
| AVar x => ALit (s x)
| APlus a1 a2 => APlus (const_prop a1 s) (const_prop a2 s)
| AMinus a1 a2 => AMinus (const_prop a1 s) (const_prop a2 s)
| AMult a1 a2 => AMult (const_prop a1 s) (const_prop a2 s)
end.

Example test1: const_prop (AVar "X"%string) aState = ALit 2.
Proof.
  simpl. reflexivity.
Qed.

Example test2: const_prop (APlus (AVar "X"%string) (AVar "Y"%string)) aState = APlus (ALit 2) (ALit 3).
Proof.
  simpl. reflexivity.
Qed.

Lemma const_prop_sound:
  forall a:aexp, forall s:state, aeval a s = aeval (const_prop a s) s.
Proof.
  induction a. auto. simpl. auto. simpl. auto. simpl. auto. simpl. auto.
Qed.