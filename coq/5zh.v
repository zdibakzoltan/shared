Inductive aexp : Type :=
| ALit (n : nat)
| APlus (a1 a2 : aexp)
| AMinus (a1 a2 : aexp)
| AMult (a1 a2 : aexp).

Fixpoint aeval (a: aexp) : nat :=
match a with
| ALit n => n
| APlus a1 a2 => aeval a1 + aeval a2
| AMinus a1 a2 => aeval a1 - aeval a2
| AMult a1 a2 => aeval a1 * aeval a2
end.

Fixpoint opt_1mul (a: aexp) : aexp :=
match a with
| ALit n => ALit n
| AMult (ALit 1) a2 => opt_1mul a2
| APlus a1 a2 => APlus (opt_1mul a1) (opt_1mul a2)
| AMinus a1 a2 => AMinus (opt_1mul a1) (opt_1mul a2)
| AMult a1 a2 => AMult (opt_1mul a1) (opt_1mul a2)
end.

Example test1: opt_1mul (AMult (ALit 1) (ALit 2)) = ALit 2.
simpl. reflexivity.
Qed.

Example test2: opt_1mul (AMult (ALit 1) (AMult (ALit 1) (ALit 2))) = ALit 2.
simpl. reflexivity.
Qed.

Example test3: opt_1mul (APlus (ALit 1) (AMult (ALit 1) (ALit 2))) = APlus (ALit 1) (ALit 2).
simpl. reflexivity.
Qed.

Theorem opt_1mul_sound:
  forall a:aexp, aeval a = aeval (opt_1mul a).
Proof.
  intros. induction a.
  simpl. reflexivity.

  destruct a1.
    destruct n.
    simpl. apply IHa2. simpl. rewrite IHa2. trivial.

  simpl in IHa1.  simpl. rewrite IHa1. rewrite IHa2. reflexivity.

  simpl. simpl in *. rewrite IHa1. rewrite IHa2. reflexivity.
  
  remember (AMult a1_1 a1_2) as a1.
  simpl. simpl in *. rewrite IHa1. rewrite IHa2. reflexivity.

  simpl. rewrite IHa1. rewrite IHa2. reflexivity.
  
Qed.