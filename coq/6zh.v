From Coq Require Import Strings.String.
Definition ident : Type := string.

Inductive aexp : Type :=
| ALit  (n : nat)
| AVar  (x : ident)
| APlus (a1 a2 : aexp).

Definition X:ident := "X"%string.
Definition Y:ident := "Y"%string.
Definition Z:ident := "Z"%string.

Fixpoint bclosed (a:aexp) : bool :=
match a with
| ALit n => true
| APlus a1 a2 => bclosed a1 && bclosed a2
| _ => false
end.

Example ex1: bclosed (ALit 1) = true.
Proof.
  simpl.  reflexivity.
Qed.

Example ex2: bclosed (AVar X) = false.
Proof.
  simpl. reflexivity.
Qed.

Example ex3: bclosed (APlus (ALit 1) (AVar Y)) = false.
Proof.
  simpl. reflexivity.
Qed.