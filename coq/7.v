From Coq Require Import Strings.String.
Definition ident : Type := string.

Inductive aexp : Type :=
| ALit  (n : nat)
| AVar  (x : ident)
| APlus (a1 a2 : aexp)
| AMult (a1 a2 : aexp).

Definition state : Type := ident -> nat.

Fixpoint aeval (a: aexp)(s: state) : nat :=
match a with
| ALit n => n
| AVar x => s x
| APlus a1 a2 => aeval a1 s + aeval a2 s
| AMult a1 a2 => aeval a1 s * aeval a2 s
end.

Definition X:ident := "X"%string.
Definition Y:ident := "Y"%string.
Definition Z:ident := "Z"%string.

Fixpoint bclosed (a:aexp): bool :=
match a with
| ALit _ => true
| AVar _ => false
| APlus a1 a2 => bclosed a1 && bclosed a2
| AMult a1 a2 => bclosed a1 && bclosed a2
end.

From Coq Require Import Bool.

Inductive closed : aexp -> Prop :=
  Cl_Lit n:
  closed (ALit n)
| Cl_Plus a1 a2:
  closed a1 -> closed a2 -> closed (APlus a1 a2)
| Cl_Mult a1 a2:
  closed a1 -> closed a2 -> closed (AMult a1 a2)
.

(* A state in which all variables are equal to zero. *)
Definition zero_state : state :=
  fun _ => 0.

(* This function replaces every variable with a zero literal *)
Fixpoint zero_exp (a:aexp): aexp :=
match a with
| ALit n => ALit n
| AVar x => ALit 0
| APlus a1 a2 => APlus (zero_exp a1) (zero_exp a2)
| AMult a1 a2 => AMult (zero_exp a1) (zero_exp a2)
end.

(* These examples help you see how zero_exp shall work *)
Example zero_exp_1:
  zero_exp (ALit 42) = ALit 42.
Proof.
 simpl. reflexivity.
Qed.

Example zero_exp_2:
  zero_exp (AVar X) = ALit 0.
Proof.
  simpl. reflexivity.
Qed.

Example zero_exp_3:
  zero_exp (AMult (ALit 42) (AVar Y)) = AMult (ALit 42) (ALit 0).
Proof.
  simpl. reflexivity.
Qed.

(* Zeroed expressions are independent of the state *)
Lemma zero_stateindep:
  forall a: aexp, forall s: state,
  aeval a zero_state = aeval (zero_exp a) s.
Proof.
  intros. induction a. simpl. reflexivity.
  simpl. split. simpl in *. rewrite IHa1. rewrite IHa2. reflexivity.
  simpl in *. rewrite IHa1. rewrite IHa2. reflexivity. 
Qed.

Definition aState : state :=
  fun x =>
    match x with
    | "X"%string => 2
    | "Y"%string => 3
    | _ => 0 (* default value for all (global) variables *)
    end.

Reserved Notation "c f=> n" (at level 50).

Inductive SOS_fstep : aexp * state -> nat -> Prop :=
| lit_eval n s:
  (ALit n, s) f=> n
| var_eval x s:
  (AVar x, s) f=> s x
| plus_eval_rightside_final a2 s i n:
  (a2, s) f=> i -> (APlus (ALit n) a2, s) f=> (n + i)
where "c f=> n" := (SOS_fstep c n).

Example ex1: (AVar X, aState) f=> 2.
Proof.
  constructor 2.
Qed.

Example ex2: (APlus (ALit 42) (AVar X), aState) f=> 44.
Proof.
  apply (plus_eval_rightside_final _ _ _ 42).
  apply var_eval.
Qed.

Reserved Notation "c i=> c'" (at level 50).
Inductive SOS_istep : aexp * state -> aexp * state -> Prop :=
| eval_plus_l_intermediate a1 a2 s a1':
  (a1, s) i=> (a1', s) -> ((APlus a1 a2), s) i=> ((APlus a1' a2), s)
| eval_plus_l_final a1 s i a2:
  (a1, s) f=> i -> ((APlus a1 a2), s) i=> ((APlus (ALit i) a2), s)
| eval_plus_r_intermediate a2 s a2' n:
  (a2, s) i=> (a2', s) -> ((APlus (ALit n) a2), s) i=> ((APlus (ALit n) a2'), s)
where "c i=> c'" := (SOS_istep c c').

Example ex3:
  (APlus (AVar X) (AVar Y), aState) i=>
  (APlus (ALit 2) (AVar Y), aState).
Proof.
  apply eval_plus_l_final.
  apply var_eval.
Qed.