From Coq Require Import Strings.String.
Definition ident : Type := string.

Inductive aexp : Type :=
| ALit (n : nat)
| AVar (x : ident)
| APlus (a1 a2 : aexp).

Definition X:ident := "X"%string.
Definition Y:ident := "Y"%string.
Definition Z:ident := "Z"%string.

Definition state : Type := ident -> nat.

Reserved Notation "c -=> n" (at level 90).
Inductive eval_bigstep : aexp * state -> nat -> Prop :=
| eval_lit n s:
  (ALit n, s) -=> n
| eval_var x s:
  (AVar x, s) -=> s x
| eval_plus a1 a2 n m s:
  (a1, s) -=> n ->
  (a2, s) -=> m ->
  (APlus a1 a2, s) -=> n + m
where "c -=> n" := (eval_bigstep c n).

Definition aState : state :=
  fun x =>
    match x with
    | "X"%string => 2
    | "Y"%string => 3
    | _ => 0 (* default value for all (global) variables *)
    end.

Definition update(s:state)(x:ident)(n:nat): state :=
  fun y =>
    if eqb x y then n else s y.

Definition anotherState : state :=
  update aState X 11.

Example ex1: (APlus (AVar X) (ALit 5), anotherState) -=> 16.
Proof.
  apply (eval_plus _ _ (anotherState X) 5 _).
  apply eval_var.
  apply eval_lit.
Qed.

Example der (s:state): (APlus (ALit 10) (ALit 5), s) -=> 15.
Proof.
apply (eval_plus _ _ 10 5 _).
apply eval_lit.
apply eval_lit.
Qed.

Inductive closed : aexp -> Prop :=
  Cl_Lit n:
  closed (ALit n)
| Cl_Plus a1 a2:
  closed a1 -> closed a2 -> closed (APlus a1 a2).

Lemma bigstep_deterministic:
  forall a:aexp, forall s:state, forall n m:nat,
  (a,s) -=> n -> (a,s) -=> m -> n = m.
Proof.
  intros. generalize dependent m.
  induction H.
    intros. inversion H0. reflexivity.
    intros. inversion H0. reflexivity.
    intros. inversion H1. rewrite (IHeval_bigstep1 n0 H6).
    rewrite (IHeval_bigstep2 m1 H7). reflexivity.
Qed.

Lemma closed_stateindep_bigstep:
  forall a:aexp, forall s s':state, forall i:nat,
  closed a -> (a, s) -=> i <-> (a, s') -=> i.
Proof.
intros.
generalize dependent i.
induction H.
intros.
split.
intros.
inversion H.
apply eval_lit.
intros.
inversion H.
apply eval_lit.
split.
intros.
inversion H1.
apply eval_plus.
apply IHclosed1.
apply H6.
apply IHclosed2.
apply H7.
intros.
inversion H1.
apply eval_plus.
apply IHclosed1.
apply H6.
apply IHclosed2.
apply H7.
Qed.