Set Warnings "-notation-overridden,-parsing".
From Coq Require Import Bool.Bool.
From Coq Require Import Init.Nat.
From Coq Require Import Strings.String.

Inductive aexp : Type :=
  | ALit (n : nat)
  | AVar (x : string)
  | APlus (a1 a2 : aexp)
  | AMinus (a1 a2 : aexp)
  | AMult (a1 a2 : aexp).

Inductive bexp : Type :=
  | BTrue
  | BFalse
  | BEq (a1 a2 : aexp)
  | BLe (a1 a2 : aexp)
  | BNot (b : bexp)
  | BAnd (b1 b2 : bexp).

Inductive stmt : Type :=
  | SSkip
  | SAssign (x:string) (a:aexp)
  | SSeq    (s1 s2: stmt)
  | SIf     (b:bexp) (s1 s2: stmt)
  | SWhile  (b: bexp) (s: stmt).

Definition X : string := "X".
Definition Y : string := "Y".
Definition Z : string := "Z".

Definition state : Type := string -> nat.
Definition empty : state := fun x => 0.
Definition update (x : string)(n : nat)(s : state): state := fun x' => if eqb x x' then n else s x'.

Fixpoint aeval (a : aexp) (st : state) : nat :=
  match a with
  | ALit n => n
  | AVar x => st x
  | APlus a1 a2 => (aeval a1 st) + (aeval a2 st)
  | AMinus a1 a2 => (aeval a1 st) - (aeval a2 st)
  | AMult a1 a2 => (aeval a1 st) * (aeval a2 st)
  end.

Fixpoint beval (b : bexp) (st : state) : bool :=
  match b with
  | BTrue => true
  | BFalse => false
  | BEq a1 a2 => (aeval a1 st) =? (aeval a2 st)
  | BLe a1 a2 => (aeval a1 st) <=? (aeval a2 st)
  | BNot b1 => negb (beval b1 st)
  | BAnd b1 b2 => andb (beval b1 st) (beval b2 st)
  end.

Fixpoint eval (niter: nat) (s: stmt) (st: state): state :=
  match niter with
    | O => st
    | S niter' =>
      match s with
      | SSkip => st
      | SAssign x a => update x (aeval a st) st
      | SSeq s1 s2 => eval niter' s2 (eval niter' s1 st)
      | SIf b s1 s2 => if (beval b st)
                       then eval niter' s1 st
                       else eval niter' s2 st
      | SWhile b s => if (beval b st)
                      then eval niter' (SWhile b s) (eval niter' s st)
                      else st
      end
    end.

Definition stmt1: stmt :=
  SAssign X (ALit 42).

Example ex1: (eval 1024 stmt1 empty) X = 42.
simpl. unfold update. simpl. reflexivity.
Qed.

Definition inf_iter: stmt :=
  SWhile BTrue SSkip.

Eval compute in (eval 1024 inf_iter empty) X.

Definition inf_iter2: stmt :=
  SWhile BTrue (SAssign X (APlus (AVar X) (ALit 1))).

Eval compute in (eval 1024 inf_iter2 empty) X.
