(* BEGIN FIX *)
Inductive bexp : Type :=
  | BTrue
  | BFalse
  | BAnd (b b' : bexp)
  | BOr  (b b' : bexp).

Fixpoint beval (b : bexp) : bool :=
  match b with
  | BTrue => true
  | BFalse => false
  | BAnd b b' => beval b && beval b'
  | BOr  b b' => beval b || beval b'
  end.

Inductive bstep : bexp -> bool -> Prop :=
  | btrue  : bstep BTrue true
  | bfalse : bstep BFalse false
  | band (b b' : bexp)(x x' : bool) :
      bstep b x ->
      bstep b' x' ->
      bstep (BAnd b b') (x && x')
  | bor (b b' : bexp)(x x' : bool) :
      bstep b x ->
      bstep b' x' ->
      bstep (BOr b b') (x || x').

Lemma denot_if_bigstep : forall b x, bstep b x -> beval b = x.
(* END FIX *)
Proof.
  intro b. induction b.
  (* BTrue *) simpl. intros. inversion H. reflexivity.
  (* BFalse *) simpl. intros. inversion H. reflexivity.
  (* BAnd *)  simpl. intros. inversion H. rewrite (IHb1 x0 H2).  rewrite (IHb2 x' H4). reflexivity.
  (* BFalse *)  simpl. intros. inversion H. rewrite (IHb1 x0 H2).  rewrite (IHb2 x' H4). reflexivity.
Qed.
---------------------------------------------------------------------------------------------------------
Lemma denot_if_bigstep : forall b x, bstep b x -> beval b = x.
(* END FIX *)
Proof.
  intros. induction H. simpl. reflexivity.
  simpl. reflexivity. simpl.
  rewrite IHbstep1. rewrite IHbstep2. reflexivity. simpl.
  rewrite IHbstep1. rewrite IHbstep2. reflexivity.
Qed.